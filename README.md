# Commsaurus

Commsaurus is a command line interface for a thesaurus API 

## Installation

Use the package manager [npm](https://pip.pypa.io/en/stable/) to install commsaurus.

```bash
npm i -g commsaurus
```

## Usage
commsaurus [option] [word]
### Options:
-   -V, --version,  outputs the version number
-  -s, --synonym,  Looks for synonyms
-  -a, --antonym,  Looks for antonyms
-  -r, --related,  Looks for related words
-  -u, --user,     Looks for user-submitted words
-  -i, --similar,  Looks for similar
### Example
```bash
commsaurus -s river
```
### Output
```bash
Synonyms for noun river: stream, watercourse,
There are no synonyms for adjective river :(
There are no synonyms for verb river :(
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)

#### Thesaurus service provided by words.bighugelabs.com
