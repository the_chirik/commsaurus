#! /usr/bin/env node

//import minimist = require("minimist");
//import * as path from 'path';

import fetch from "node-fetch";
//const clear = require('clear');
//const path = require('path');
//const program = require('commander');
//import * asstNat from "request-promise-native";
//const API_key = "Get an api key @ https://words.bighugelabs.com/api.php";
interface SynCategory {
  syn: string[];
  ant: string[];
  rel: string[];
  sim: string[];
  usr: string[];
}

interface SynResult {
  noun: SynCategory;
  verb: SynCategory;
  adjective: SynCategory;
  adverb: SynCategory;
}

export class Commsaurus {
  apiKey: String;
  constructor(apiKey: String) {
    if (apiKey) {
      this.apiKey = apiKey;
    } else {
      throw new Error("No api key passed");
    }
  }
  async find(_word: string): Promise<SynResult> {
    const res = await fetch(`http://words.bighugelabs.com/api/2/${this.apiKey}/${_word}/json`);
    return res.json()
    
  }
  synonymVerb(word: string): Promise<string[]> {
    //var found = this.find(word);
    try{
      return this.find(word).then((data: SynResult) => data.verb.syn);
    }
    catch(TypeError){
      throw new Error(TypeError);
    }
    
  }
  antonymVerb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.verb.ant);
  }
  similarVerb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.verb.sim);
  }
  relatedVerb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.verb.rel);
  }
  userVerb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.verb.usr);
  }
  synonymNoun(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.noun.syn);
  }
  antonymNoun(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.noun.ant);
  }
  similarNoun(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.noun.sim);
  }
  relatedNoun(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.noun.rel);
  }
  userNoun(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.noun.usr);
  }
  synonymAdjective(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adjective.syn);
  }
  antonymAdjective(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adjective.ant);
  }
  similarAdjective(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adjective.sim);
  }
  relatedAdjective(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adjective.rel);
  }
  userAdjective(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adjective.usr);
  }
  synonymAdverb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adverb.syn);
  }
  antonymAdverb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adverb.ant);
  }
  similarAdverb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adverb.sim);
  }
  relatedAdverb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adverb.rel);
  }
  userAdverb(word: string): Promise<string[]> {
    return this.find(word).then((data: SynResult) => data.adverb.usr);
  }
}

/*clear();
console.log("Welcome to commsaurus. We can find synonyms, antonyms, similar words, related words, and user-submitted words for your noun/verb/adjective\nUsage:\n");
program
  .version('0.0.1')
  .description("Commsaurus - a command line thesaurus.")
  .option('-s, --synonym', 'Look for synonyms')
  .option('-a, --antonym', 'Look for antonyms')
  .option('-r, --related', 'Look for related words')
  .option('-u, --user', 'Look for user submitted words')
  .option('-i, --similar', 'Look for similar')
  .parse(process.argv);


console.log('you ordered a pizza with:');
if (program.args) {
//  word = program.args;
}
//if (program.synomym) console.log(synonymAll(program.args[0]));
if (program.adjective) console.log('  - pineapple');
if (program.noun) {
  Commsaurus.synonymVerb("cool").then((data) => {
    var output:String = "Synonyms for Verb cool: ";
    var j: any;
    for(j in data) {
      output+=data[j]+" ";
    }
    console.log(output);
  });
  console.log('  - bbq');
}
if (program.general){
//  console.log(str);

} 
*/