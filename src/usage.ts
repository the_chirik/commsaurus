#! /usr/bin/env node

"use strict";
// const clear = require('clear');
//const path = require('path');
const program = require('commander');
import { Commsaurus } from './index';
const API_KEY = "5eaca353b91940ff2f027661162781e8";

let syn = new Commsaurus(API_KEY);

program
  .version('1.4.2')
  .description("\n\n\n\n\n\n\n\n\n\n\n\n\nWelcome to commsaurus,a command line thesaurus. We can find synonyms, antonyms, similar words, related words, and user-submitted words for your noun/verb/adjective/adverb\nUsage:\n$ commsaurus [option] [word]")
  .option('-s, --synonym', 'Look for synonyms')
  .option('-a, --antonym', 'Look for antonyms')
  .option('-r, --related', 'Look for related words')
  .option('-u, --user', 'Look for user submitted words')
  .option('-i, --similar', 'Look for similar')
  .parse(process.argv);
if (!process.argv.slice(2).length) {
    program.outputHelp();
}
if (program.synonym) {
    console.log();
    syn.synonymVerb(program.args).then((data) => {
        var output:String = "Synonyms for verb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no synonyms for this verb. Try looking for -r (related) or -i (similar)."));
    syn.synonymNoun(program.args).then((data) => {
        var output:String = "Synonyms for noun "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no synonyms for this noun. Try looking for -r (related) or -i (similar)."));
    syn.synonymAdjective(program.args).then((data) => {
        var output:String = "Synonyms for adjective "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no synonyms for this adjective. Try looking for -r (related) or -i (similar)."));
    syn.synonymAdverb(program.args).then((data) => {
        var output:String = "Synonyms for adverb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no synonyms for this adverb. Try looking for -r (related) or -i (similar)."));
}
if (program.antonym) {
    console.log();
    syn.antonymVerb(program.args).then((data) => {
        var output:String = "Antonyms for verb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no antonyms for verb "+program.args+" :("));
    syn.antonymNoun(program.args).then((data) => {
        var output:String = "Antonyms for noun "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no antonyms for noun "+program.args+" :("));
    syn.antonymAdjective(program.args).then((data) => {
        var output:String = "Antonyms for adjective "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no antonyms for adjective "+program.args+" :("));
    syn.antonymAdverb(program.args).then((data) => {
        var output:String = "Antonyms for adverb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no antonyms for adverb "+program.args+" :("));
    console.log();
}
if (program.similar) {
    console.log();
    syn.similarVerb(program.args).then((data) => {
        var output:String = "Similar words for verb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no similar words for this verb. Try looking for -s (synonyms) or -r (related)."));
    syn.similarNoun(program.args).then((data) => {
        var output:String = "Similar words for noun "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no similar words for this noun. Try looking for -s (synonyms) or -r (related)."));
    syn.similarAdjective(program.args).then((data) => {
        var output:String = "Similar words for adjective "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no similar words for this adjective. Try looking for -s (synonyms) or -r (related)."));
    syn.similarAdverb(program.args).then((data) => {
        var output:String = "Similar words for adverb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no similar words for this adverb. Try looking for -s (synonyms) or -r (related)."));
}
if (program.related) {
    console.log();
    syn.relatedVerb(program.args).then((data) => {
        var output:String = "Related words for verb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no related words for this verb. Try looking for -s (synonyms) or -i (similar)."));
    syn.relatedNoun(program.args).then((data) => {
        var output:String = "Related words for noun "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no related words for this noun. Try looking for -s (synonyms) or -i (similar)."));
    syn.relatedAdjective(program.args).then((data) => {
        var output:String = "Related words for adjective "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no related words for this adjective. Try looking for -s (synonyms) or -i (similar)."));
    syn.relatedAdverb(program.args).then((data) => {
        var output:String = "Related words for adverb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no related words for this adverb. Try looking for -s (synonyms) or -i (similar)."));
}
if (program.user) {
    console.log();
    syn.userVerb(program.args).then((data) => {
        var output:String = "User words for verb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no user words for verb "+program.args+" :("));
    syn.userNoun(program.args).then((data) => {
        var output:String = "User words for noun "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no user words for noun "+program.args+" :("));
    syn.userAdjective(program.args).then((data) => {
        var output:String = "User words for adjective "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no user words for adjective "+program.args+" :("));
    syn.userAdverb(program.args).then((data) => {
        var output:String = "User words for adverb "+program.args+": "
        var j: any;
        for(j in data) {
            output+=data[j]+", ";
        }
        console.log(output);
    }).catch(() =>console.log("There are no user words for adverb "+program.args+" :("));
}