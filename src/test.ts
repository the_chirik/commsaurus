"use strict";
import { Commsaurus } from './index';
const API_KEY = "5eaca353b91940ff2f027661162781e8";

let syn = new Commsaurus(API_KEY);




syn.synonymVerb("cool").then((data) => {
  var output:String = "Synonyms for Verb cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.antonymVerb("cool").then((data) => {
  var output:String = "Antonyms for Verb cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.similarVerb("cool").then((data) => {
  var output:String = "Similars for Verb cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.relatedVerb("cool").then((data) => {
  var output:String = "Relateds for Verb cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.userVerb("cool").then((data) => {
  var output:String = "Users for Verb cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.synonymNoun("cool").then((data) => {
  var output:String = "Synonyms for Noun cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.antonymNoun("cool").then((data) => {
  var output:String = "Antonyms for Noun cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.similarNoun("cool").then((data) => {
  var output:String = "Similars for Noun cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.relatedNoun("cool").then((data) => {
  var output:String = "Relateds for Noun cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.userNoun("cool").then((data) => {
  var output:String = "Users for Noun cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.synonymAdjective("cool").then((data) => {
  var output:String = "Synonyms for Adjective cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.antonymAdjective("cool").then((data) => {
  var output:String = "Antonyms for Adjective cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.similarAdjective("cool").then((data) => {
  var output:String = "Similars for Adjective cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.relatedAdjective("cool").then((data) => {
  var output:String = "Relateds for Adjective cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});
syn.userAdjective("cool").then((data) => {
  var output:String = "Users for Adjective cool: ";
  var j: any;
  for(j in data) {
    output+=data[j]+" ";
  }
  console.log(output);
});